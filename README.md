# Getting Started 
## Overview
This is a guide that helps you understand how to use your PiHat

### Using your piHat

- Connect your 12V battery into the battery holder
- Using a USB, Connect the power supply to your piHat
- Once you have done this, you should notice LEDs swithing on.
- Your PiHat is now ready to be used 
